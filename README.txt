UMass Global Branding
=====================

UMass Global Branding module swaps in a global header and footer from external markup, css, and js resources.

This module takes no input, loads external static files, and has zero configuration.

See umass_global_branding.module file for location of external static resources.

Clear the site cache to reload and refresh header.

Cached global branding resources will expire and reload every 24 hours.

Include the following CSS for any custom pages which should not have branding.

    #umass--global--header, #umass--global--footer {
      display: none;
    }

Authors
=======

David Ruderman <david.ruderman@umass.edu>
Brian Devore <bdevore@umass.edu>
